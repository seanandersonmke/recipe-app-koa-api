"use strict";
// const fb = require('./fbConfig');
// console.log(fb);
const koa = require("koa");
const koaRouter = require("koa-router");

const app = new koa();
const router = new koaRouter();

router.get("koala", "/config", ctx => {
  ctx.body = "Welcome! To the Koala Book of Everything!";
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(4000, () => console.log("running on port 4000"));