import api from "../api";
import admin from "firebase-admin";

var USER;

api.post("/auth", async ctx => {
  console.log("hit auth");
  try {
    const token = await admin.auth().verifyIdToken(ctx.request.body.token);
    if (token) {
      USER = {
        uid: token.uid,
        exp: token.exp
      };
      ctx.status = 201;
      ctx.body = {
        status: "success",
        data: token
      };
      console.log(token);
    } else {
      ctx.status = 400;
      ctx.body = {
        status: "error",
        message: "Something went wrong."
      };
    }
  } catch (err) {
    console.log(err);
  }
});

export default api;