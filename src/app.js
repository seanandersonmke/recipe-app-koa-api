import Koa from "koa";
import api from "./api";
// import config from './config';
import bodyParser from "koa-bodyparser";
import cors from "kcors";

const app = new Koa()
  .use(cors())
  // .use(async (ctx, next) => {
  //   ctx.state.collections = config.collections;
  //   ctx.state.authorizationHeader = `Key ${config.key}`;
  //   await next();
  // })
  .use(bodyParser())
  .use(api.routes())
  .use(api.allowedMethods());

export default app;

// "use strict";
//
// require("firebase/database");
// const koa = require("koa"),
//   koaRouter = require("koa-router"),
//   firebase = require("./fbConfig.js"),
//   serviceAccount = require("./recipe-app-195017-firebase-adminsdk-awkfa-5deca5d91a.json"),
//   bodyParser = require('koa-bodyparser'),
//   admin = require("firebase-admin"),
//   db = firebase.database(),
//   app = new koa(),
//   newRecipe = require("./routes/newrecipe"),
//   authRoute = require("./routes/auth"),
//   logger = require('koa-logger'),
//   router = new koaRouter();

// let _USER = module.exports = {};

//
// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL: "https://recipe-app-195017.firebaseio.com"
// });
//
// app.use(bodyParser());
// app.use(router.routes()).use(router.allowedMethods());
// app.use([newRecipe, authRoute]);
// app.use(logger());
// app.listen(8080, () => console.log("running on port 8080"));
//
// module.exports = router;