import firebase from "firebase/app";
import admin from "firebase-admin";
import {
  FBCONFIG
} from "./firebaseapp";
const serviceAccount = require("../recipe-app-195017-firebase-adminsdk-awkfa-5deca5d91a.json");

export default function initFb() {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://recipe-app-195017.firebaseio.com"
  });

  firebase.initializeApp(FBCONFIG);
}