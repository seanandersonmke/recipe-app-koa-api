import initFb from "./fbConfig";
import firebase from "firebase/app";
require("firebase/database");

initFb();

export default class Db {
  constructor(uid) {
    this.baseUrl = `users/${uid}`;
    this.db = firebase.database();
  }
  push(endpoint, obj) {
    const call = this.db.ref(this.baseUrl + endpoint).push(obj);
    return call.key;
  }
}