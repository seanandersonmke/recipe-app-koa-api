import KoaRouter from 'koa-router';
import admin from "firebase-admin";
import Db from "./database";
const api = KoaRouter();

let database,
  key;

api.post("/auth", async ctx => {
  try {
    const token = await admin.auth().verifyIdToken(ctx.request.body.token);
    if (token) {
      ctx.status = 201;
      ctx.body = {
        status: "success"
      };
      database = new Db(token.uid);
    } else {
      ctx.status = 400;
      ctx.body = {
        status: "error",
        message: "Something went wrong."
      };
    }
  } catch (err) {
    console.log(err);
  }
});

api.post("/newrecipe", async ctx => {
  let title = ctx.request.body.title;

  ctx.status = 201;
  ctx.body = {
    status: "success"
  };

  let data = {
    recipe: {
      title: title
    }
  }

  key = database.push("/recipes", data);
});



export default api;